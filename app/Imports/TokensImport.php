<?php

namespace App\Imports;

use DB;
use Session;
use App\FormToken;
use App\UsedFunction;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class TokensImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $check = FormToken::where('token',$row['PIN Nos.'])->first();

        if($check==null){
            return new FormToken([
                //
                'form_code' => $row['College Name'],
                'pin_batch' => $row['PIN Batch ID'],
                'pin_expiry' => $row['Expiry Date'],
                'token' => $row['PIN Nos.'],
                'upload_batch' => Session::get('batch')
            ]);
        }
        else{
            return $check;
        }
    }
}

<?php

namespace App;
use DB;
use App\User;

class UsedFunction
{
    //
    public function notify(string $telephone,string $email,string $name,string $message)
    {
    	$procedureName = 'prc_notifications';
    	$empty = '';
    	$header = 'ETAC';

    	if($telephone != '')
    	{
			$bindings = [
			     'contact_v' => $telephone,
				  'receiver_name' => $name,
				  'header_v' => $header,
				  'message' => $message,
				  'notification_type' => 'SMS',
				  'destination_type' => 'EXT'
			];

			DB::executeProcedure($procedureName, $bindings); 
		}

		if($email != '')
		{
			$bindings = [
			     'contact_v' => $email,
				  'receiver_name' => $name,
				  'header_v' => $header,
				  'message' => $message,
				  'notification_type' => 'EMAIL',
				  'destination_type' => 'EXT'
			];

			 DB::executeProcedure($procedureName, $bindings); 
		}


    }

    public function sendEtac(string $id)
    {
    	//get user details
    	$user = User::findOrFail($id);


    	if($user){
    		$code = $this->genEtac();
    		//generate code theo
    		$user->USER_UNI_ID = substr($this->oracleHash($code),1,20);
    		$user->save();
    		$message = 'Your Request has been recieved. Your Authorization PIN to complete your Request is: '.$code. ' .  Thank You.';
    		$this->notify($user->telno,$user->email1,$user->user_alias,$message);

    	}
    }

    public function validateEtac(string $id,string $code)
    {
    	$user = User::find($id);

    	if($user){
    		$etac = $user->user_uni_id;
    		$coded = substr($this->oracleHash($code),1,20);
    		
    		if($etac==$coded)
    		{
    			return true;
    		}
    		else
    		{
    			return false;
    		}
    	}
    }

    public function genEtac() {
        $chars = '0123456789';
        $code = '';
        for ( $i = 0; $i < 5; $i++ ) {
          $code .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $code;
    }

    protected function oracleHash($password)
    {        
        $result = DB::selectOne("select return_hash('$password') as value from dual");
        return $result->value;       
    }

    public static function get_batchno(){
        $result = DB::selectOne("select get_batchno as value from dual");
        return $result->value;  
    }

    public static function randomHex() {
        $chars = 'ABCDEF0123456789';
        $color = '#';
        for ( $i = 0; $i < 6; $i++ ) {
          $color .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $color;
    }
}

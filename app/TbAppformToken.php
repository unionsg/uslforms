<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TbAppformToken extends Model
{
    //
    protected $table = 'tb_appform_token';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'form_code', 'pin_batch', 'pin_expiry','token','upload_batch',
    ];
}

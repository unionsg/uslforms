<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\TokensImport;
use App\TbSchool;
use App\FormToken;
use App\TbAppformToken;
use App\VwFormtokenSum;
use App\UsedFunction;
use Alert;
use Session;
use Excel;
use DB;

class TokenController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $schools = TbSchool::all();

        return view('admin.tokens.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request;
        $batch = UsedFunction::get_batchno();
        Session::put('batch', $batch);

        

        if($request->hasFile('input_file'))
         {            
             $file = $request->file('input_file');    
            $ext = $file->getClientOriginalExtension();             
             $name=$file->getClientOriginalName().'-'.$batch.'.'.$ext;

            // return response()->json(['hh'=> Excel::import(new TokensImport, $file)]);
            set_time_limit(400);
             if(Excel::import(new TokensImport, $file))
             {
                $file->move(storage_path().'/tokenupload/', $name); 
             }
             
         }
         else{
            return 'No file';
         }

         $data = FormToken::where('upload_batch',$batch)->get();
         $datasum = VwFormtokenSum::where('upload_batch',$batch)->get();

         // return $data;

       return view('admin.tokens.upload',['datas'=>$data,
                                          'datasums'=>$datasum,
                                          'batch'=>$batch
                                ]);

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request)
    {
      
        $data = FormToken::where('upload_batch',$request->batch)
                            ->select(array('form_code', 'pin_batch', 'pin_expiry','token','upload_batch'));

        $load = DB::table('tb_appform_token')
                         ->insertUsing([ 'form_code', 'pin_batch', 'pin_expiry','token','upload_batch'], $data);
        
   
       
        if($load)
        {
            $del = FormToken::where('upload_batch',$request->batch)
                             ->delete();
            if($del)
            {
                Alert::success('Success ','Batch succeessfuly uploaded');
                return redirect()->route('home');
            }           
        }

      // return $data;
        Alert::success('Error ','Error uploading batch');
        return redirect()->route('upload-token');
       // return view('admin.tokens.upload',['datas'=>$data,
       //                                    'datasums'=>$datasum,
       //                                    'batch'=>$batch
       //                          ]);
       
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request)
    {
      
       
        $del = FormToken::where('upload_batch',$request->batch)
                             ->delete();
        if($del)
        {
             Alert::success('Success ','Batch succeessfuly rejected');
            return redirect()->route('upload-token');
        }           
      

       
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;
use App\IntStatement;
use Request;
use Auth;
use DB;

class AccountController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

  
    public function acct_trans()
    {
    	$inputs = Request::all();

    	$dinfo = [
    			'from' => '',
    			'to' => ''
    		];

    	//	return $dinfo;

    	$acct = $inputs['acct'];
    	$startdate = '';
    	$enddate = '';
    	$identifier = 'tv'.$acct;
    	$err = '';
    	$limit = '10';


    	$procedureName = 'mbank_gen_estatement';

		$bindings = [
		     'AC_NUM' => $acct,
			  'STARTDATE' => $startdate,
			  'ENDDATE' => $enddate,
			  'TRANS_LIMIT' => $limit,
			  'REP_IDENTIFIER' => $identifier,
			  'ERROR_MSG' => $err
		];

		// $bindings = [
		//      'AC_NUM' => $acct,
		// 	  'STARTDATE' => $startdate,
		// 	  'ENDDATE' => $enddate			 
		// ];

		$result = DB::executeProcedure($procedureName, $bindings);

		//return $acct;

		//dd($result);


		if($err == '')
		{
			$res = IntStatement::where('addr',$identifier);
			$data = $res->get();
			$dbSum = $res->sum('amount_debit');
			$crSum = $res->sum('amount_credit');

			//get grap data
			$chartjs = app()->chartjs
	        ->name('TransStat')
	        ->type('pie')
	        ->size(['width' => 400, 'height' => 200])
	        ->labels(['TotalDebit','TotalCredit'])
	        ->datasets([
	            [
	            	"label" => "My First dataset",
	                'backgroundColor' => ['red','blue'],
	                'hoverBackgroundColor' => ['red','blue'],
	                'data' => [$dbSum,$crSum]
	            ]
	        ])
	        ->options([]);

	        $acct_info = DB::table('vw_tv_useraccounts')
                         ->where([                               
                                ['USER_ID', '=', Auth::id()],
                                ['acct_id',$acct]
                                ])                       
                        ->first();

			//return $crSum;
			return view('details',['trans'=>$data,
								   'chartjs'=>$chartjs,
								   'acct_info' => $acct_info,
								   'dinfo'=> $dinfo,
								   'dfrom'=>$startdate,
								   'dto' => $enddate
								]);
		}
		else{
			return $err;
		}

  //       $startdate = $inputs['startdate'];
  //       $enddate = $inputs['enddate'];
  //       $branch = $inputs['branch'];

  //   	$result = DB::connection('ora_db')->select(
		//     DB::raw("exec PRW_GENERA_FACTURA(:val1)"), array ('val1' => $val1)
		// );
    }

    public function acct_dtrans()
    {
    	$inputs = Request::all();

    	//return $inputs;
    	$acct = $inputs['acct'];
    	$startdate = $inputs['from'];
    	$enddate = $inputs['to'];
    	$identifier = 'tv'.$acct;
    	$err = '';
    	$limit = '';


    	$procedureName = 'mbank_gen_estatement';

		$bindings = [
		     'AC_NUM' => $acct,
			  'STARTDATE' => $startdate,
			  'ENDDATE' => $enddate,
			  'TRANS_LIMIT' => $limit,
			  'REP_IDENTIFIER' => $identifier,
			  'ERROR_MSG' => $err
		];

		// $bindings = [
		//      'AC_NUM' => $acct,
		// 	  'STARTDATE' => $startdate,
		// 	  'ENDDATE' => $enddate			 
		// ];

		$result = DB::executeProcedure($procedureName, $bindings);

		

		if($err == '')
		{
			$res = IntStatement::where('addr',$identifier);
			$data = $res->get();
			$dbSum = $res->sum('amount_debit');
			$crSum = $res->sum('amount_credit');

			//get grap data
			$chartjs = app()->chartjs
	        ->name('TransStat')
	        ->type('pie')
	        ->size(['width' => 400, 'height' => 200])
	        ->labels(['TotalDebit','TotalCredit'])
	        ->datasets([
	            [
	            	"label" => "My First dataset",
	                'backgroundColor' => ['red','blue'],
	                'hoverBackgroundColor' => ['red','blue'],
	                'data' => [$dbSum,$crSum]
	            ]
	        ])
	        ->options([]);

	        $acct_info = DB::table('vw_tv_useraccounts')
                         ->where([                               
                                ['USER_ID', '=', Auth::id()],
                                ['acct_id',$acct]
                                ])                       
                        ->first();

			//return $inputs;
			return view('details',['trans'=>$data,
								   'chartjs'=>$chartjs,
								   'acct_info' => $acct_info,
								   'dfrom'=>$startdate,
								   'dto' => $enddate
								]);
		}
		else{
			return $err;
		}

  //       $startdate = $inputs['startdate'];
  //       $enddate = $inputs['enddate'];
  //       $branch = $inputs['branch'];

  //   	$result = DB::connection('ora_db')->select(
		//     DB::raw("exec PRW_GENERA_FACTURA(:val1)"), array ('val1' => $val1)
		// );
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;
use App\UsedFunction;
use App\Imports\TokensImport;
use Excel;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function randomHex() {
        $chars = 'ABCDEF0123456789';
        $color = '#';
        for ( $i = 0; $i < 6; $i++ ) {
          $color .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $color;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      
      $sum = DB::table('VW_FORMSALES_SUM')
            ->get();

     $stat = DB::table('VW_FORMSALES_STATS')
            ->get();
        
        if(Auth::user()->float_account!=null){
            return redirect()->route('post-trans');
        }

        return view('home',['sums'=>$sum,'stats'=>$stat]);
       
    }

    public function upload()
    {
        
        $batch = UsedFunction::get_batchno();

        Session::put('batch', $batch);

        $load =  Excel::import(new TokensImport, storage_path('tok.xlsx'));
        if ($load) {
            # code...
            return $batch;
        }
       
        return $load;
         return view('home')->with('success', 'Insert Record successfully.');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importExcel(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]);

        Excel::import(new TokensImport, request()->file('your_file'));
    }
}

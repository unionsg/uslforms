<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TbSchool;
use Auth;
Use Alert;
use DB;
use PDO;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
          $data = DB::table('vw_appform_trans')
                    ->whereNotNull('local_equivalent_db')
                    ->get();

        return view('admin.transaction.view',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Request $request)
    {
        //
         $startdate = $request->from;
        $enddate = $request->to;

         $data = DB::table('vw_appform_trans_all')
                    ->whereBetween('posting_date',[$startdate,$enddate])
                    ->whereNotNull('local_equivalent_db')
                    ->get();
        
        return view('admin.transaction.view',['data'=>$data]);
    }

     /**
     * Print the reciept of the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print($id)
    {
        //
        $data = DB::table('vw_appform_trans_all')
                    ->where('token',base64_decode($id))
                    ->whereNotNull('local_equivalent_db')
                    ->first();
        if($data!=null)
        {
            return view('admin.transaction.print',['data'=>$data]);
        }
         Alert::error('Error ', 'Invalid id');
         return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storesch(Request $request)
    {
        //
           if($request->hasFile('input_file'))
         {            
             $file = $request->file('input_file');    
            $ext = $file->getClientOriginalExtension();             
             $name=$file->getClientOriginalName().'-'.$batch.'.'.$ext;
            $file_data = base64_encode(file_get_contents($file));

             // return response()->json(['hh'=> Excel::import(new TokensImport, $file)]);

             $sch = TbSchool::find('001');

             return $sch;
         }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
Use Alert;
use DB;
use PDO;
class TransController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = DB::table('vw_appform_trans')
                    ->where('user_name',Auth::user()->user_name)
                    ->whereNotNull('local_equivalent_db')
                    ->get();

        return view('transaction.view',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                
        $floatAccount = Auth::user()->float_account;
       // return $floatAccount;
        if($floatAccount == '')
        {            
            Alert::error('Error','Not a valid user to post transactions');
            return back()->with('error','no float account for user');
        }

        $forms = DB::table('tb_appform_setup')
                    ->where('status','A')
                    ->get();

       $sums = DB::table('vw_appform_trans_sum')
                    ->where('user_name',Auth::user()->user_name)
                    ->get();
        
        $coh = DB::table('vw_appform_trans_sum')
                    ->where('user_name',Auth::user()->user_name)
                    ->sum('amount');

        return view('transaction.create',['forms'=>$forms,
                                        'sums'=>$sums,
                                        'coh'=>$coh]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $debitAccount = Auth::user()->float_account;
        $formCode = $request->formcode;
        $paidBy = $request->paidby;
        $telephone = $request->telephone;
        $telephone1 = $request->telephone1;
        $postedBy = Auth::user()->user_name;
        $ip = $request->ip();
        $channel = env('CHANNEL');
        $ext = "";
       
       if($telephone!=$telephone1){
         Alert::error('Error ', 'Telephone number mismatch');
         return back();
       }


        $pdo = DB::getPdo();       

        $stmt = $pdo->prepare("begin prc_appform_sales(:p1, :p2, :p3,:p4,:p5,:p6,:p7,:p8, :p9, :p10,:p11,:p12,:p13,:p14,:p15,:p16); end;");
        $stmt->bindParam(':p1', $debitAccount, PDO::PARAM_STR);
        $stmt->bindParam(':p2', $formCode, PDO::PARAM_STR);
        $stmt->bindParam(':p3', $paidBy, PDO::PARAM_STR);
        $stmt->bindParam(':p4', $ext, PDO::PARAM_STR);
        $stmt->bindParam(':p5', $telephone, PDO::PARAM_STR);
        $stmt->bindParam(':p6', $postedBy, PDO::PARAM_STR);
        $stmt->bindParam(':p7', $postedBy, PDO::PARAM_STR);
        $stmt->bindParam(':p8', $ip, PDO::PARAM_STR);
        $stmt->bindParam(':p9', $ip, PDO::PARAM_STR);
        $stmt->bindParam(':p10', $channel, PDO::PARAM_STR);
        $stmt->bindParam(':p11', $ext, PDO::PARAM_STR);
        $stmt->bindParam(':p12', $ext, PDO::PARAM_STR);
        $stmt->bindParam(':p13', $ext, PDO::PARAM_STR);
        $stmt->bindParam(':p14', $ext, PDO::PARAM_STR);
        $stmt->bindParam(':p15', $docRef, PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->bindParam(':p16', $error, PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->execute();

        if($stmt)
        {            
            if($error!=""){
                 Alert::error('Error ', $error);
                return back()->with('error',$error);
            }

            $schDetails = DB::table('vw_appforms_all')
                            ->where('form_code',$formCode)
                            ->first();
            
           // return $recptDetails;
           return redirect()->route('print', ['id' => base64_encode($docRef)]);
        }

      
        return $error." ".$docRef." ".$ip;

     }


    /**
     * Print the reciept of the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print($id)
    {
        //
        $data = DB::table('vw_appform_trans_all')
                    ->where([
                        ['user_name',Auth::user()->user_name],
                        ['acct_link',Auth::user()->float_account],
                        ['token',base64_decode($id)]
                    ])
                    ->first();
        if($data!=null)
        {
            return view('transaction.print',['data'=>$data]);
        }
         Alert::error('Error ', 'Invalid id');
         return back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $startdate = $request->from;
        $enddate = $request->to;

         $data = DB::table('vw_appform_trans_all')
                    ->where('user_name',Auth::user()->user_name)
                    ->whereBetween('posting_date',[$startdate,$enddate])
                    ->get();

        return view('transaction.view',['data'=>$data]);       
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAmount(Request $request)
    {
        //
        $formCode = $request->formCode;

         $data = DB::table('tb_appform_setup')
                    ->where('form_code',$formCode)
                    ->value('amount');

        return $data;       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

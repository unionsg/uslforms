<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TbSchool extends Model
{
    //
    protected $primaryKey = 'school_code';
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes(['verify'=>true]);
Route::get('logout', 'Auth\LoginController@logout');
Route::post('secsetup','Auth\SetupPasswordController@loginSetup')->name('setup');
Route::post('seclogin','Auth\SetupPasswordController@etacLogin')->name('etac-login');


Route::get('upload-token','TokenController@create')->name('upload-token');
Route::post('upload-token','TokenController@store');
Route::post('reject-upload','TokenController@reject');
Route::post('confirm-upload','TokenController@confirm');

Route::post('sch-create','AdminController@storesch');
Route::get('sch-create','AdminController@create');
Route::get('view-alltrans','AdminController@index');
Route::post('view-alltrans','AdminController@view');
Route::get('/admin-print/{id}','AdminController@print')->name('admin-print');

//USL form sales
Route::get('post-trans','TransController@create')->name('post-trans');
Route::post('post-trans','TransController@store');
Route::get('view-trans','TransController@index');
Route::post('view-trans','TransController@show');
Route::post('form-det','TransController@getAmount');
Route::get('/print/{id}','TransController@print')->name('print');

//Dangote Payments
Route::get('post-dp', 'DangoteController@createDp')->name('post-dp');
Route::get('post-atc', 'DangoteController@createAtc')->name('post-atc');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/upload', 'HomeController@upload')->name('upload');
Route::get('/home', 'HomeController@index')->name('home');


Route::post('acct-det','AccountController@acct_trans')->name('account-trans');
Route::post('acct-ddet','AccountController@acct_dtrans')->name('account-dtrans');
@extends('layouts.master')

@section('styles')

@endsection

@section('content')

 <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <!-- <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">CRM</a></li>
                                            <li class="breadcrumb-item active">Contacts</li>
                                        </ol>
                                    </div> -->
                                    <h4 class="page-title">Post Transaction</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 


                        <div class="row">
                            <div class="col-xl-8">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row mb-2">
                                            <div class="col-sm-4">
                                               <!--  <form class="form-inline">
                                                    <div class="form-group mb-2">
                                                        <label for="inputPassword2" class="sr-only">Search</label>
                                                        <input type="search" class="form-control" id="inputPassword2" placeholder="Search...">
                                                    </div>
                                                </form> -->
                                            </div>
                                            <div class="col-sm-8">
                                               <!--  <div class="text-sm-right">
                                                    <button type="button" class="btn btn-success waves-effect waves-light mb-2 mr-1"><i class="mdi mdi-settings"></i></button>
                                                    <a href="#custom-modal" class="btn btn-danger waves-effect waves-light mb-2" data-animation="fadein" data-plugin="custommodal" data-overlayColor="#38414a"><i class="mdi mdi-plus-circle mr-1"></i> Add Contact</a>
                                                </div> -->
                                            </div><!-- end col-->
                                        </div>
                
                                        <div class="mb-3">
                                        	<form method="POST" action="{{url('post-trans')}}" autocomplete="off">                                        		
                                              <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
	                                           <div class="form-group">
	                                               <label>Select Form Type</label>
	                                               <select class="form-control" name="formcode" id="formcode" required="" onchange="myFunction()">
	                                               	<option value=""> -- Select form type --</option>
	                                               	@if(isset($forms))
	                                               		@foreach($forms as $form)
	                                               			<option value="{{$form->form_code}}">{{$form->form_type}}</option>
	                                               		@endforeach
	                                               	@endif
	                                               </select>	                                               
	                                           </div>
	                                           <div class="form-group">
	                                               <label>Amount</label>
	                                               <input type="text" class="form-control" name="amount" id="amount" readonly="">
	                                           </div>
	                                           <div class="form-group">
	                                               <label>Paid By</label>
	                                               <input type="text" class="form-control" id="paidby" name="paidby" >
	                                           </div>
	                                           <div class="form-group">
	                                               <label>Telephone</label>
	                                               <input type="password" class="form-control" id="telephone" name="telephone" >
	                                           </div>
                                                <div class="form-group">
                                                   <label>Confirm Telephone</label>
                                                   <input type="number" class="form-control" id="telephone1" name="telephone1" >
                                               </div>
	                                           
                                               <button type="reset" class="btn btn-danger waves-effect waves-light float-left">Reset</button>
	                                           <button type="submit" class="btn btn-primary waves-effect waves-light float-right">Submit</button>
	                                       </form>
                                        </div>

                                        
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div> <!-- end col -->

                            <div class="col-xl-4">
                                <div class="card-box">
                                    <div class="media">
                                        <!-- <img class="d-flex mr-3 rounded-circle avatar-lg" src="assets/images/users/user-8.jpg" alt="Generic placeholder image"> -->
                                        <div class="media-body">
                                            <h4 class="mt-0 mb-1">SLL {{number_format($coh,2, '.', ',')}}</h4>
                                            <p class="text-muted">Cash on hand</p>
                                            <!-- <p class="text-muted"><i class="mdi mdi-office-building"></i> Vine Corporation</p> -->

                                           <!--  <a href="javascript: void(0);" class="btn- btn-xs btn-info">Send Email</a>
                                            <a href="javascript: void(0);" class="btn- btn-xs btn-secondary">Call</a>
                                            <a href="javascript: void(0);" class="btn- btn-xs btn-secondary">Edit</a> -->
                                        </div>
                                    </div>

                                    <h5 class="text-uppercase bg-light p-2"><i class="mdi mdi-account-circle mr-1"></i> SUMMARY</h5>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>FORM TYPE</th>
                                                <th style="text-align: center;">COUNT</th>
                                                <th style="text-align: center">AMOUNT</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($sums))
                                            @foreach($sums as $sum)
                                            <tr>
                                                <td>{{$sum->form_code}}</td>
                                                <td style="text-align: center">{{$sum->records}}</td>
                                                <td style="text-align: right;">{{number_format($sum->amount,2, '.', ',')}}</td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                  <!--   <div class="">
                                        <h4 class="font-13 text-muted text-uppercase">About Me :</h4>
                                        <p class="mb-3">
                                            Hi I'm Johnathn Deo,has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley of type.
                                        </p>

                                        <h4 class="font-13 text-muted text-uppercase mb-1">Date of Birth :</h4>
                                        <p class="mb-3"> March 23, 1984 (34 Years)</p>

                                        <h4 class="font-13 text-muted text-uppercase mb-1">Company :</h4>
                                        <p class="mb-3">Vine Corporation</p>

                                        <h4 class="font-13 text-muted text-uppercase mb-1">Added :</h4>
                                        <p class="mb-3"> April 22, 2016</p>

                                        <h4 class="font-13 text-muted text-uppercase mb-1">Updated :</h4>
                                        <p class="mb-0"> Dec 13, 2017</p>

                                    </div>
 -->
                                </div> <!-- end card-box-->
                            </div>
                        </div>
                        <!-- end row -->
                        
                    </div> <!-- container -->

@endsection

@section('scripts')
<script type="text/javascript">
	function myFunction() {
      var token = $('#token').val();
      var formCode = $('#formcode').val();

       $.ajax({
        type: "POST",        
        data: "formCode=" + formCode  + "&_token=" + token,       
         url: "{{url('form-det')}}",
        success:function(data){
            console.log(data);
             $('#amount').val(data);
        }    
       });
	  
	}

  $(document).ready(function() {
    $('#telephone').bind("cut copy paste", function(e) {
        e.preventDefault();
         alert("You cannot copy/paste into this text field.");
        $('#telephone').bind("contextmenu", function(e) {
            e.preventDefault();
        });
    });

     $('#telephone1').bind("cut copy paste", function(e) {
        e.preventDefault();
         alert("You cannot copy/paste into this text field.");
        $('#telephone1').bind("contextmenu", function(e) {
            e.preventDefault();
        });
    });
});

</script>
@endsection

   
@extends('layouts.master')

@section('styles')
 <link href="{{asset('assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />
 <link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<!-- Start Content-->
<div class="container-fluid">

	 <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control border-white" id="dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                            <i class="mdi mdi-autorenew"></i>
                        </a>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                            <i class="mdi mdi-filter-variant"></i>
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Application Form Token Upload</h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            @if(!isset($datasums))
                	<form method="POST" action="{{url('sch-create')}}" autocomplete="off" enctype="multipart/form-data">
                	                     @csrf
                	     <div class="form-group mb-3">
                	         <label for="file">Upload Excel File</label>
                             <input type="file" class="dropify" id="input_file" name="input_file" data-height="50" required="" />
                	         <!-- <input id="input_file" type="file" class="form-control" name="input_file" required placeholder="Upload excel file"> -->
                	     </div>

                	     <div class="form-group mb-0 text-center">
                	         <button class="btn btn-primary btn-block" type="submit"> Upload </button>
                	     </div>

                	 </form>
                      @else
                        <h3>NOTE</h3>
                        <ul>
                            <li>Please ensure data uploaded is same as record uploaded</li>
                            <li>Press the confirm button to make tokens available for sale</li>
                        </ul>
                      @endif
                    </div>
                    <div class="col-md-6">
                       @if(isset($datasums))
                       <h3>Summary</h3>
                        <table style="margin-left: 30px;line-height: 35px;margin-top: 20px">
                           @foreach($datasums as $datasum)
                            <tr>
                                <td style="font-weight: bold;">{{$datasum->form_code}}</td>
                                <td style="padding-left: 20px;padding-right: 20px">:</td>
                                <td>{{$datasum->total}}</td>
                            </tr>
                            @endforeach
                        </table>
                        <div id="reject" class="float-left">
                            <form method="POST" action="{{url('reject-upload')}}">
                                @csrf
                                <input type="hidden" name="batch" value="{{$batch}}">
                                <button type="submit" class="btn btn-danger">REJECT</button>
                            </form>
                        </div>

                         <div id="confirm" class="float-right">
                            <form method="POST" action="{{url('confirm-upload')}}">
                                @csrf
                                <input type="hidden" name="batch" value="{{$batch}}">
                                <button type="submit" class="btn btn-primary ">CONFIRM</button>
                            </form>
                        </div>
                        
                        @endif
                    </div>
                </div>
                 </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

     @isset($datas)
        <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                     <div class="table-responsive">
                        <table id="dataTable" class="table mb-0 dt-responsive">
                            <thead class="thead-light">
                            <tr>
                                <th>Form Code</th>
                                <th>Pin Batch</th>
                                <th>Pin Expiry</th>
                                <th>Pin</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($datas as $data)
                                 <tr>
                                    <td>{{$data->form_code}}</td>
                                    <td>{{$data->pin_batch}}</td>
                                    <td>{{$data->pin_expiry}}</td>
                                    <td>{{$data->token}}</td>
                                 </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->  
                   
                 </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

     <div class="row">
       
        <div class="col-md-6">
             
        </div>
    </div>
    @endif

</div>


@endsection

@section('scripts')
 
    <script src="{{asset('assets/libs/dropify/dropify.min.js')}}"></script>
     <script src="{{asset('assets/js/pages/form-fileuploads.init.js')}}"></script>
     <script src="{{asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script type="text/javascript">
     $(document).ready( function () {
        $('#dataTable').DataTable({
            dom: 'Bfrtip',
            lengthChange: !1,
            // buttons: ["print", "csv","pdf"],
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
    } );
</script>

@endsection
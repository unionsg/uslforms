<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #000000;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body onLoad="print_report()">
        <div class="flex-center position-ref full-height" id="paper">
          
            <div class="content">
                @if(isset($data))
                <img class="img-thumbnail float-left" style="height: 80px;padding-top: 20px" src="{{url('/image/slcb_big.png')}}">
                <img class="img-rounded float-right" style="width:100px" src="{{url('/image/usl.jpg')}}">
            
               <p><b>APPLICATION FORMS</b></p>
              
               <p>{{$data->school_name}}</p>
               <p>{{$data->form_type}}</p>
               <p>Amount : {{number_format($data->local_equivalent_db,2, '.', ',')}}</p>
               <p>Pin : <b>{{$data->token}}</b></p>
               <p>Trnx Date : {{$data->posting_sys_date}}</p>
                @endif
            </div>
        </div>

    <script>
    //print Function
    function print_report() {

        var printDiv = document.getElementById('paper');
        var newWin = window.open("", 'Print Window', 'width=800,height=500,top=50,left=100');
//        newWin.document.open();
        newWin.document.write('<html><head><title>Reciept</title><link href="css/bootstrap.min.css" rel="stylesheet"><href="css/main.css" rel="stylesheet"></head><body onload="window.print()">' + printDiv.innerHTML + '</body></html>');
        newWin.document.close();
        setTimeout(function() {
            newWin.close();
             window.location = "{{url('/view-alltrans')}}";
        }, 2000);
       

    }

    </script>
    </body>
</html>

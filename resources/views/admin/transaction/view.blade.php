@extends('layouts.master')

@section('styles')
 <link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>
@endsection

@section('content')

 <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <!-- <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">CRM</a></li>
                                            <li class="breadcrumb-item active">Contacts</li>
                                        </ol>
                                    </div> -->
                                    <h4 class="page-title">View Transaction</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 


                        <div class="row">
                        	 <div class="col-xl-2">
                                <div class="card-box">
                                   <h5 class="text-uppercase bg-light p-2"><i class="mdi mdi-account-circle mr-1"></i> SEARCH</h5>

                                   <form method="POST" action="{{url('view-alltrans')}}" id="transSearch">
                                       	@csrf
                                       	<div class="form-group">
                                       	    <label>Start Date</label>
                                       	     <input type="text" id="datefrom" name="from" class="form-control" required="" value="">
                                       	</div>
                                       	<div class="form-group">
                                       	    <label>End Date</label>
                                       	    <input type="text" class="form-control" name="to" id="dateto" required="" value="">
                                       	</div>
                                       	 <div class="form-group">
                                             <button type="submit" class="btn btn-primary btn-block waves-effect waves-light mb-2">Search</button>
                                         </div>
                                   </form>

                                   <!-- <h5 class="text-uppercase bg-light p-2"><i class="mdi mdi-account-circle mr-1"></i> NOTE</h5> -->
                                                                     
                                </div> <!-- end card-box-->
                            </div>

                            <div class="col-xl-10">
                                <div class="card">
                                    <div class="card-body">
                                       
                                        <table class="table mb-0 dt-responsive" id="transTable">
                                        	<thead>
                                        		<tr>
                                        			<th>POSTING DATE</th>
                                        			<th>TRANSACTION DETAILS</th>
                                        			<!-- <th>REFERENCE</th> -->
                                        			<th>AMOUNT</th>
                                        			<th>FORM CODE</th>
                                        			<th>TELEPHONE</th>
                                                    <th>ACTION</th>
                                        		</tr>
                                        	</thead>
                                        	<tbody>
                                        		@if(isset($data))
                                        			@foreach($data as $dat)
                                        				<tr>
                                        					<td>{{$dat->posting_date}}</td>
                                        					<td>{{$dat->transaction_details}}</td>
                                        					<!-- <td>{{$dat->document_ref}}</td> -->
                                        					<td>{{number_format($dat->local_equivalent_db,2, '.', ',')}}</td>
                                        					<td>{{$dat->form_code}}</td>
                                        					<td>{{$dat->telephone}}</td>
                                                            <td><a class="btn btn-sm btn-success" href="{{url('/admin-print/'.base64_encode($dat->token))}}">PRINT</a></td>
                                        				</tr>
                                        			@endforeach
                                        		@endif
                                        	</tbody>
                                        </table>
                                        
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div> <!-- end col -->
                           
                        </div>
                        <!-- end row -->
                        
                    </div> <!-- container -->

@endsection

@section('scripts')
<script src="{{asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<!-- <script src="{{asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script> -->
<!-- <script src="{{asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script> -->
<script src="{{asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<!-- <script src="{{asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script> -->
<script src="{{asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<!-- <script src="{{asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script> -->
<script src="{{asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script type="text/javascript">
     ! function(e) {
    "use strict";
    var i = function() {};
    i.prototype.init = function() {
        e("#datefrom").flatpickr({
            altInput: !0,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d"
        }), e("#dateto").flatpickr({
            altInput: !0,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d"
        })
    }, e.FormPickers = new i, e.FormPickers.Constructor = i
}(window.jQuery),
function(e) {
    "use strict";
    e.FormPickers.init()
}(window.jQuery);


    $(document).ready( function () {
        $('#transTable').DataTable({
            dom: 'Bfrtip',
            lengthChange: !1,
            buttons: ["print", "csv","pdf"],
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
    } );
 </script>
<script type="text/javascript">
	function myFunction() {
	  var x = document.getElementById("formcode").value;
	  document.getElementById("amount").value =  x;
	}


    $( "#transSearch" ).submit(function( event ) {
        var x = document.getElementById("datefrom").value;
        var y = document.getElementById("dateto").value;

        if(x===''){
            alert("Please specify the start date");
            return false;
        }else if(y===''){
            alert("Please specify the end date");
            return false;
        } else{
        return true;
       }
      // event.preventDefault();
    });

	function validate(){
		var x = document.getElementById("datefrom").value;
		var y = document.getElementById("dateto").value;

        alert(x);
		if(x===''){
			alert('hmmmm');
			return false;
		}else if(y===''){
			return false;
		} else{
		return true;
	   }
	}
</script>
@endsection
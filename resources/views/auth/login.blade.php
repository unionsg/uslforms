@extends('layouts.app')

@section('content')

 <div class="col-md-4 col-lg-4 col-xl-4 card" style="display: inline-block;position: fixed;top: 0;bottom: 0;left: 0;right: 0;width: auto;height: 390px;margin: auto;background-color: #ffffffd1">

<div class="card-body">                           
          
        <h4 class="header">
            <i class="fa fa-lock"> </i>
            AUTHENTICATION
        </h4>

   <form method="POST" action="{{ route('login') }}" autocomplete="off">
                        @csrf

        <div class="form-group mb-3">
            <label for="emailaddress">Email address</label>
             <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Enter your email">
              @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
        </div>

        <div class="form-group mb-3">
            <label for="password">Password</label>
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Enter your password">

             @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group mb-3">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} >
                <label class="custom-control-label" for="checkbox-signin">Remember me</label>
            </div>
        </div>

        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary btn-block" type="submit"> Log In </button>
        </div>

    </form>

    <div class="text-center">
        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif       
    </div>

</div> <!-- end card-body -->

</div>


@endsection

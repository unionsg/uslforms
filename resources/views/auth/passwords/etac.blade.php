@extends('layouts.app')

@section('content')
<div class="col-md-3 col-lg-3 col-xl-3 card" style="display: inline-block;position: fixed;top: 0;bottom: 0;left: 0;right: 0;width: auto;height: 400px;margin: auto;background-color: #ffffffd1">

<div class="card-body p-4">

    <h4 class="header">
          Welcome {{Auth::user()->user_alias}} <br>
          Enter Transaction Code (ETAC)
     </h4>

     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{url('seclogin')}}">
        @csrf

         
         <div class="form-group mb-3">
            <!-- <label for="emailaddress">Confirm Password</label> -->
              <input id="password" type="password" class="form-control" name="etac"required placeholder="ETAC">
            
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors}}</strong>
                    </span>
             
        </div>


        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary btn-block" type="submit"> <span class=" fa fa-key"></span> Verify </button>
        </div>

         <div class="text-center">
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Resend Code') }}
            </a>
        </div>
    </form>

    <p>
       <span class="fa fa-info-circle"></span> <span style="font-weight: bold;color: red">Please Note: </span> 
       To proceed, you will need an Electronic Transaction Code (eTAC). Your eTAC will be sent to your mobile phone via SMS
    </p>
</div> <!-- end card-body -->

</div>


@endsection


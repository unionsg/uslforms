@extends('layouts.app')

@section('content')
<div class="col-md-4 col-lg-4 col-xl-4 card" style="display: inline-block;position: fixed;top: 0;bottom: 0;left: 0;right: 0;width: auto;height: 500px;margin: auto;background-color: #ffffffd1">

<div class="card-body p-4">

     <p >        
         <h4 class="header"> <i class="fa fa-lock"> </i> Security Setup</h4>
         Please change your password and fill security details for future use
     </p>


     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{url('secsetup')}}">
        @csrf

         <div class="form-group mb-3">
            <!-- <label for="emailaddress">New Password</label> -->
              <input id="password" type="password" class="form-control" name="password"  required placeholder="New Password">
        </div>

         <div class="form-group mb-3">
            <!-- <label for="emailaddress">Confirm Password</label> -->
              <input id="password" type="password" class="form-control" name="password1"required placeholder="Confirm Password">
            
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors}}</strong>
                    </span>
             
        </div>

         <div class="form-group mb-3">
            <!-- <label for="emailaddress">Security Question</label> -->
            <select class="form-control" name="sec" required >
                <option value=""> -- Select Security Question -- </option>
                @foreach($ques as $question)
                <option value="{{$question->q_code}}">{{$question->q_description}}</option>
                @endforeach
            </select>
        </div>

          <div class="form-group mb-3">
            <!-- <label for="emailaddress">Security Answer</label> -->
              <input id="secanswer" type="text" class="form-control" name="secanswer" required placeholder="Security Answer">            
        </div>

        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary btn-block" type="submit"> Confirm </button>
        </div>


    </form>
</div> <!-- end card-body -->

</div>


@endsection


@extends('layouts.app')

@section('content')
 <div class="col-md-4 col-lg-4 col-xl-4 card" style="display: inline-block;position: fixed;top: 0;bottom: 0;left: 0;right: 0;width: auto;height: 390px;margin: auto;background-color: #ffffffd1">

<div class="card-body p-4">

     <h4 class="header">
            <i class="fa fa-lock"> </i>
            Password Reset
        </h4>

     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        @csrf

         <div class="form-group mb-3">
            <label for="emailaddress">Email address</label>
              <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
              @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
        </div>

        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary btn-block" type="submit"> {{ __('Send Password Reset Link') }} </button>
        </div>

        <div class="text-center">
        @if (Route::has('login'))
            <a class="btn btn-link" href="{{ route('login') }}">
                {{ __('Sign in') }}
            </a>
        @endif       
    </div>


    </form>
</div> <!-- end card-body -->

</div>


@endsection


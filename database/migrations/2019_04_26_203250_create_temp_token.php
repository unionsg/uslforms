<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_code');
            $table->string('pin_batch');
            $table->string('pin_expiry');
            $table->string('token');
            $table->string('upload_batch');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_tokens');
    }
}
